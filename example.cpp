#include <mavsdk/mavsdk.h>
#include <mavsdk/plugins/action/action.h>
#include <mavsdk/plugins/mission/mission.h>
#include <mavsdk/plugins/telemetry/telemetry.h>

#include <functional>
#include <future>
#include <iostream>
#include <memory>
#include <cmath>

#define ERROR_CONSOLE_TEXT "\033[31m"
#define TELEMETRY_CONSOLE_TEXT "\033[34m"
#define NORMAL_CONSOLE_TEXT "\033[0m"

using namespace mavsdk;
using namespace std::chrono;
using namespace std::this_thread;

inline void handle_action_err_exit(Action::Result result, const std::string& message);
inline void handle_mission_err_exit(Mission::Result result, const std::string& message);
inline void handle_connection_err_exit(ConnectionResult result, const std::string& message);

void usage(std::string bin_name)
{
    std::cout << NORMAL_CONSOLE_TEXT << "Usage : " << bin_name << std::endl
              << "For example, to connect to the simulator use URL: udp://:14540" << std::endl;
}

int main(int argc, char** argv)
{
    Mavsdk dc;
    std::string connection_url;
    ConnectionResult connection_result;

    connection_url = "udp://:14540";

    if (argc >= 2) {
        connection_url = argv[1];
    }

    std::cout << "Connection URL: " << connection_url << std::endl;

    {
        auto prom = std::make_shared<std::promise<void>>();
        auto future_result = prom->get_future();

        std::cout << "Waiting to discover system..." << std::endl;
        dc.register_on_discover([prom](uint64_t uuid) {
            std::cout << "Discovered system with UUID: " << uuid << std::endl;
            prom->set_value();
        });

        connection_result = dc.add_any_connection(connection_url);
        handle_connection_err_exit(connection_result, "Connection failed: ");

        future_result.get();
    }

    dc.register_on_timeout([](uint64_t uuid) {
        std::cout << "System with UUID timed out: " << uuid << std::endl;
        std::cout << "Exiting." << std::endl;
        exit(0);
    });

    System& system = dc.system();
    auto action = std::make_shared<Action>(system);
    auto mission = std::make_shared<Mission>(system);
    auto telemetry = std::make_shared<Telemetry>(system);

    while (!telemetry->health_all_ok()) {
        std::cout << "Waiting for system to be ready" << std::endl;
        sleep_for(seconds(1));
    }

    std::cout << "System ready" << std::endl;

    std::cout << "Arming..." << std::endl;
    const Action::Result arm_result = action->arm();
    handle_action_err_exit(arm_result, "Arm failed: ");
    std::cout << "Armed." << std::endl;

    std::cout << "wait 15 sec......" << std::endl;
    sleep_for(seconds(2));
    std::cout << "END WAITING" << std::endl;
    
    {
        std::cout << "Commanding TAKEOFF..." << std::endl;
        const Action::Result result = action->takeoff();
        if (result != Action::Result::SUCCESS) {
            std::cout << "Failed to command TAKEOFF (" << Action::result_str(result) << ")"
                      << std::endl;
        } else {
            std::cout << "Commanded TAKEOFF." << std::endl;
        }
    }

    {
        std::cout << "Commanding GOTO..." << std::endl;
        const Action::Result result = action->goto_location(48.874336, 2.407231, 30.0f, NAN);
        if (result != Action::Result::SUCCESS) {
            std::cout << "ERRORS !!!!! " << std::endl;
        }
    }

    while (true) {
        auto battery = telemetry->battery();
        auto position = telemetry->position();
        std::cout << "Battery remaining -> " << battery.remaining_percent * 100 << "%" << std::endl;
        std::cout << "position -> lat: " << position.latitude_deg << ", long: " << position.longitude_deg << ", alt: " << position.absolute_altitude_m << std::endl;
        std::cout << "alt relative to home altitude: " << position.relative_altitude_m << std::endl;
        sleep_for(seconds(1));
    }

    {
        std::cout << "Commanding RTL..." << std::endl;
        const Action::Result result = action->return_to_launch();
        if (result != Action::Result::SUCCESS) {
            std::cout << "Failed to command RTL (" << Action::result_str(result) << ")"
                      << std::endl;
        } else {
            std::cout << "Commanded RTL." << std::endl;
        }
    }

    return 0;
}

inline void handle_action_err_exit(Action::Result result, const std::string& message)
{
    if (result != Action::Result::SUCCESS) {
        std::cerr << ERROR_CONSOLE_TEXT << message << Action::result_str(result)
                  << NORMAL_CONSOLE_TEXT << std::endl;
        exit(EXIT_FAILURE);
    }
}

inline void handle_connection_err_exit(ConnectionResult result, const std::string& message)
{
    if (result != ConnectionResult::SUCCESS) {
        std::cerr << ERROR_CONSOLE_TEXT << message << connection_result_str(result)
                  << NORMAL_CONSOLE_TEXT << std::endl;
        exit(EXIT_FAILURE);
    }
}
